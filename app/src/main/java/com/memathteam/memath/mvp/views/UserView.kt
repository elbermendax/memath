package com.memathteam.memath.mvp.views

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.memathteam.memath.data.Achievement

/**
 * 04/03/2018 11:17
 *
 * @author elber.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface UserView : MvpView {

    fun setName(name: String)

    fun setPhoto(imgUrl: String)

    fun setUserProgressInfo(lvl: Int, xp: Int, nextLevelXp: Int)

    fun setProgress(percentage: Int)

    fun setAchievements(data: List<Achievement>)

    @StateStrategyType(SkipStrategy::class)
    fun logout()
}