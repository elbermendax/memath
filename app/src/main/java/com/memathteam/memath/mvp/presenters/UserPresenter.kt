package com.memathteam.memath.mvp.presenters

import android.content.SharedPreferences
import com.arellomobile.mvp.InjectViewState
import com.memathteam.memath.app.App
import com.memathteam.memath.data.Achievement
import com.memathteam.memath.data.User
import com.memathteam.memath.mvp.views.UserView
import com.memathteam.memath.repository.Repository
import com.memathteam.memath.utils.Keys
import com.vk.sdk.api.*
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.subscribers.DisposableSubscriber
import org.json.JSONObject
import timber.log.Timber
import javax.inject.Inject
import kotlin.math.pow

/**
 * 04/03/2018 11:17
 *
 * @author elber.
 */
@InjectViewState
class UserPresenter : BasePresenter<UserView>() {

    @Inject
    lateinit var prefs: SharedPreferences
    @Inject
    lateinit var repository: Repository

    init {
        App.appComponent.inject(this)
    }

    fun logout() {
        prefs.edit()
                .remove(Keys.APP_PREFERENCES_TOKEN)
                .remove(Keys.VK_USER_ID)
                .remove(Keys.VK_USER_PHOTO_URL)
                .apply()

        unsubscribeOnDestroy(
                repository.clearUsers()
                        .subscribeWith(object : DisposableCompletableObserver(){
                            override fun onComplete() {
                                viewState.logout()
                            }

                            override fun onError(e: Throwable) {  }
                        })
        )
    }

    override fun onFirstViewAttach() {
        setupUser()
        setupPhoto()
    }

    private fun setupUser() {
        val req = repository.getUser()
                .subscribeWith(object : DisposableSingleObserver<User>() {
                    override fun onSuccess(t: User) {
                        Timber.v("Setting up retrieved user: $t")
                        viewState.setName(t.name)
                        val xpPair = parseXp(t.totalXp)

                        val nextLvl = xpPair.first
                        val xpForNextLvl = xpPair.second

                        val percentage = getPercentage(t.totalXp, nextLvl)

                        viewState.setUserProgressInfo(nextLvl, t.totalXp, xpForNextLvl)
                        viewState.setProgress(percentage)
                    }

                    override fun onError(e: Throwable) {
                        Timber.e("Got error: ${e.message}")
                    }
                })

        unsubscribeOnDestroy(req)

        unsubscribeOnDestroy(
                repository.getAchievements()
                        .subscribeWith(object : DisposableSingleObserver<List<Achievement>>(){
                            override fun onSuccess(t: List<Achievement>) {
                                val res = t.filter { it.achieved }
                                if (res.isNotEmpty()) {
                                    viewState.setAchievements(res)
                                }
                            }

                            override fun onError(e: Throwable) {
                                Timber.e("Got error: ${e.message}")
                            }
                        })
        )
    }

    private fun setupPhoto(){
        val imgUrl = prefs.getString(Keys.VK_USER_PHOTO_URL, "")
        if (imgUrl != ""){
            viewState.setPhoto(imgUrl)
        } else {
            VKApi
                    .users()
                    .get(VKParameters.from(
                            VKApiConst.USER_IDS, prefs.getString(Keys.VK_USER_ID, ""),
                            VKApiConst.FIELDS, "photo_100"
                    ))
                    .executeWithListener(object : VKRequest.VKRequestListener() {
                        override fun onComplete(response: VKResponse?) {
                            val jsonResp = response?.json?.getJSONArray("response")
                            Timber.v("Received usr photo url: $jsonResp\nStarted saving it")
                            val photoUrl = (jsonResp?.get(0) as JSONObject).getString("photo_100")
                            prefs.edit().putString(Keys.VK_USER_PHOTO_URL, photoUrl).apply()
                            viewState.setPhoto(photoUrl)
                        }
                    })
        }
    }

    private fun parseXp(xp: Int) : Pair<Int, Int>{
        var lvl = 1
        while (xp >= 2.0.pow(lvl-1).toInt()*100) lvl++
        return lvl to 2.0.pow(lvl-1).toInt()*100
    }

    private fun getPercentage(totalXp: Int, nextLvl: Int) =
            (((totalXp - 2.0.pow(nextLvl-2)*100) / (2.0.pow(nextLvl-1)*100)) * 100).toInt()
}