package com.memathteam.memath.mvp.presenters

import android.content.SharedPreferences
import com.arellomobile.mvp.InjectViewState
import com.memathteam.memath.app.App
import com.memathteam.memath.utils.Keys
import com.memathteam.memath.mvp.views.SplashView
import javax.inject.Inject

/**
 * Created by elber on 27/02/2018.
 */
@InjectViewState
class SplashPresenter : BasePresenter<SplashView>() {

    @Inject
    lateinit var prefs: SharedPreferences

    init {
        App.appComponent.inject(this)
    }

    override fun onFirstViewAttach() {
        if (prefs.getString(Keys.APP_PREFERENCES_TOKEN, "") != "") {
            viewState.navigateToMainScreen()
        }
        else {
            viewState.navigateToLogin()
        }
    }
}