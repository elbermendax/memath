package com.memathteam.memath.mvp.views

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.vk.sdk.VKAccessToken

/**
 * 28/02/2018 00:23
 * Created by elber.
 */

@StateStrategyType(SkipStrategy::class)
interface LoginView : MvpView {

    fun vkSdkLogin()

    fun authorizeAtServer(token: VKAccessToken)

    fun showProgress()

    fun hideProgress()

    fun onLoginStart()

    fun onLoginSuccess()

    fun onLoginFailed()

    fun onLoginFinished()

    fun navigateToMainActivity()
}