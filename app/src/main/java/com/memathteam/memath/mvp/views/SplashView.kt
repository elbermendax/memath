package com.memathteam.memath.mvp.views

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by elber on 27/02/2018.
 */
@StateStrategyType(SkipStrategy::class)
interface SplashView : MvpView {

    fun navigateToLogin()

    fun navigateToMainScreen()
}