package com.memathteam.memath.mvp.presenters

import com.arellomobile.mvp.InjectViewState
import com.memathteam.memath.app.App
import com.memathteam.memath.mvp.views.MainView
import com.memathteam.memath.repository.Repository
import timber.log.Timber
import javax.inject.Inject

/**
 * 03/03/2018 23:57
 *
 * @author elber.
 */
@InjectViewState
class MainPresenter : BasePresenter<MainView>() {

    @Inject
    lateinit var repository: Repository

    init {
        App.appComponent.inject(this)
    }

    fun cacheData() {
        viewState.setupNavigation()

        unsubscribeOnDestroy(repository.cacheCategories())
        unsubscribeOnDestroy(repository.cacheProgress())
        unsubscribeOnDestroy(repository.setupAchievements())
    }
}