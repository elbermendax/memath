package com.memathteam.memath.mvp.presenters

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import com.arellomobile.mvp.InjectViewState
import com.memathteam.memath.app.App
import com.memathteam.memath.data.Category
import com.memathteam.memath.mvp.views.StudyView
import com.memathteam.memath.repository.Repository
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.observers.DisposableObserver
import timber.log.Timber
import javax.inject.Inject

/**
 * 04/03/2018 14:10
 *
 * @author elber.
 */
@InjectViewState
class StudyPresenter : BasePresenter<StudyView>() {

    @Inject
    lateinit var repository: Repository

    private val btnNextStudyLessonColors = arrayOf(
            ColorDrawable(Color.parseColor("#0bd200")),
            ColorDrawable(Color.parseColor("#1265f4"))
    )

    private val btnNextRepeatLessonColors = btnNextStudyLessonColors.reversedArray()
    private var isRepeat = false

    init {
        App.appComponent.inject(this)
    }

    override fun onFirstViewAttach() {
        viewState.setupLessonModeChange()
        loadThemeList()
    }

    fun startNextLesson() {
        viewState.startNextLesson()
    }

    private fun loadThemeList(){
        var tempCategories = listOf<Category>()
        val tempProgress = arrayListOf<Pair<Int,Int>>()
        var i = 0
        val res = arrayListOf<Pair<Category, Pair<Int,Int>>>()
        val req = repository.getCategories()
                .doOnNext {
                    Timber.v("Received $it")
                    tempCategories = it
                    Timber.v("Proceeding to retrieving progress...")
                }
                .flatMap { Flowable.fromIterable(it) }
                .flatMap {
                    repository.getProgressForCategory(it.id)
                }
                .toObservable()
                .subscribeWith(object : DisposableObserver<Pair<Int, Int>>() {
                    override fun onComplete() {
                        Timber.e("If you see that message, then something went wrong...")
                    }

                    override fun onNext(t: Pair<Int, Int>) {
                        Timber.v("Received $t")
                        tempProgress.add(t)

                        Timber.v("Almost done...\n$tempCategories\n$tempProgress")
                        if (i < tempCategories.size) {
                            res.add(Pair(tempCategories[i], tempProgress[i]))
                            i++
                            Timber.v("Finished composing...\n$res")
                            viewState.setThemeList(res.toList())
                        }
                    }

                    override fun onError(e: Throwable) {
                        Timber.e(e.message)
                    }
                })

        unsubscribeOnDestroy(req)
    }
}