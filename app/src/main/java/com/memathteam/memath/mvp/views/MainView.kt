package com.memathteam.memath.mvp.views

import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * 03/03/2018 23:56
 *
 * @author elber.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface MainView : MvpView{

    @StateStrategyType(SkipStrategy::class)
    fun setupNavigation()

    @StateStrategyType(SkipStrategy::class)
    fun inflateFragment(fragment: MvpAppCompatFragment)
}