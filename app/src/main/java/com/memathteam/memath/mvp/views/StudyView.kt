package com.memathteam.memath.mvp.views

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.memathteam.memath.data.Category

/**
 * 04/03/2018 14:10
 *
 * @author elber.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface StudyView : MvpView {

    fun setupLessonModeChange()

    fun startNextLesson()

    fun setThemeList(categories: List<Pair<Category, Pair<Int,Int>>>)

}