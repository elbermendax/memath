package com.memathteam.memath.mvp.presenters

import android.os.Handler
import com.arellomobile.mvp.InjectViewState
import com.memathteam.memath.app.App
import com.memathteam.memath.data.Achievement
import com.memathteam.memath.data.Task
import com.memathteam.memath.data.User
import com.memathteam.memath.data.UserProgress
import com.memathteam.memath.domain.AnswerEntry
import com.memathteam.memath.domain.LessonEntry
import com.memathteam.memath.mvp.views.LessonView
import com.memathteam.memath.repository.Repository
import com.memathteam.memath.utils.Keys
import com.memathteam.memath.utils.subscribeIoObserveMain
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableObserver
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.subscribers.DisposableSubscriber
import timber.log.Timber
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.math.pow

/**
 * 11/03/2018 18:58
 *
 * @author elber.
 */
@InjectViewState
class LessonPresenter : BasePresenter<LessonView>() {

    @Inject
    lateinit var repository: Repository

    init {
        App.appComponent.inject(this)
    }

    private var initialUserXp = 0

    private var lessonStarted = false

    private var currExp = 0
    private val initialTaskAmount = 8
    private var taskIndex = 0
    private var taskStartTime = 0L

    private var timerTotalTime = 30000L
    private var isFinished = false

    private var achievements: List<Achievement> = listOf()

    private val tasks: ArrayList<LessonEntry> = arrayListOf()

    fun startLesson() {
        unsubscribeOnDestroy(
                repository.getUser()
                        .subscribeIoObserveMain()
                        .subscribeWith(object : DisposableSingleObserver<User>(){
                            override fun onSuccess(t: User) {
                                initialUserXp = t.totalXp
                            }

                            override fun onError(e: Throwable) {
                                Timber.e("Got error: ${e.message}")
                            }
                        })
        )

        unsubscribeOnDestroy(
                repository.getAchievements()
                        .subscribeWith(object : DisposableSingleObserver<List<Achievement>>(){
                            override fun onSuccess(t: List<Achievement>) {
                                achievements = t
                            }

                            override fun onError(e: Throwable) {
                                Timber.e("Got error: ${e.message}")
                            }
                        })
        )

        unsubscribeOnDestroy(repository.getCategories()
                .flatMap { Flowable.fromIterable(it) }
                .flatMap { repository.getTasksByCategory(it.id) }
                .flatMap { Flowable.fromIterable(it) }
                .subscribeWith(object : DisposableSubscriber<Task>(){
                    override fun onComplete() {
                        Timber.v("Completed observing flowable from tasks")
                    }

                    override fun onNext(task: Task?) {
                        Timber.v("Received $task in LessonPresenter")
                        if (task != null) {
                            repository.getProgressByTaskId(task.id)
                                    .subscribeWith(object : DisposableSingleObserver<UserProgress>(){
                                        override fun onSuccess(t: UserProgress) {
                                            if (tasks.size < initialTaskAmount) {
                                                tasks.add(
                                                        LessonEntry(
                                                                task,
                                                                t,
                                                                getAnswersList(task)
                                                        )
                                                )
                                                Timber.v("${tasks.size}")
                                            }
                                            if (tasks.size == initialTaskAmount && !lessonStarted) {
                                                setupLessonInitials()
                                            }
                                        }

                                        override fun onError(e: Throwable) {
                                            if (tasks.size < initialTaskAmount) {
                                                tasks.add(
                                                        LessonEntry(
                                                                task,
                                                                UserProgress(),
                                                                if (task.type == 1) getAnswersList(task) else listOf()
                                                        )
                                                )
                                                Timber.v("${tasks.size}")
                                            }
                                            if (tasks.size == initialTaskAmount && !lessonStarted) {
                                                setupLessonInitials()
                                            }
                                        }
                                    })
                        }
                    }

                    override fun onError(t: Throwable?) {
                        Timber.e("Error while loading tasks for all categories from DB: ${t?.message}")
                    }
                })
        )
    }

    fun gotItWorkout() {
        val eval = 100 - (System.currentTimeMillis() - taskStartTime) / 80
        val xpGain = if (eval > 0) eval else 0

        currExp += xpGain.toInt()
        viewState.setNewXp(currExp)

        updateTaskProgressAndXp()

        timerTotalTime += 1000
        tasks.add(
                LessonEntry(
                        tasks[taskIndex].task,
                        UserProgress(),
                        getAnswersList(tasks[taskIndex].task)
                )
        )
        Handler().postDelayed({
            setupNextLessonEntry()
        }, 400)
    }

    fun processCorrectAnswer() {
        val eval = 100 - (System.currentTimeMillis() - taskStartTime) / 100
        val xpGain = if (eval > 0) eval else 0

        currExp += xpGain.toInt()
        viewState.setNewXp(currExp)

        updateTaskProgressAndXp()

        timerTotalTime += 3000
        tasks.add(
                LessonEntry(
                        tasks[taskIndex].task,
                        UserProgress(),
                        getAnswersList(tasks[taskIndex].task)
                )
        )
        Handler().postDelayed({
            setupNextLessonEntry()
        }, 400)
    }

    fun processWrongAnswer() {
        timerTotalTime -= 2000
        tasks.add(
                LessonEntry(
                        tasks[taskIndex].task,
                        UserProgress(),
                        if (tasks[taskIndex].task.type == 2) listOf() else getAnswersList(tasks[taskIndex].task)
                )
        )
        Handler().postDelayed({
            setupNextLessonEntry()
        }, 400)
    }

    private fun setupLessonInitials() {
        lessonStarted = true
        taskStartTime = System.currentTimeMillis()
        viewState.setNextLessonEntry(tasks[taskIndex])

        unsubscribeOnDestroy(
                Observable.interval(500, TimeUnit.MILLISECONDS)
                        .subscribeIoObserveMain()
                        .subscribeWith(object : DisposableObserver<Long>(){
                            override fun onComplete() {
                                Timber.v("Completed emitting time interval")
                            }

                            override fun onNext(t: Long) {
                                viewState.setTimerText(timerTotalTime)
                                timerTotalTime -= 500
                                if (timerTotalTime <= 0 && !isFinished) {
                                    isFinished = true
                                    processAchievements()
                                    viewState.setLessonFinishedLayout(currExp)
                                }
                            }

                            override fun onError(e: Throwable) {
                                Timber.e("Error while emitting time interval for timer")
                            }
                        })
        )
    }

    private fun processAchievements(){
        val conditions = achievements.map { it.condition }
        if (currExp > 800) {
            achievements[conditions.indexOf(Keys.KEY_ACHIEVEMENTS_1)].achieved = true
        }
        if (currExp > 1600) {
            achievements[conditions.indexOf(Keys.KEY_ACHIEVEMENTS_2)].achieved = true
        }
        if (initialUserXp + currExp >= 2.0.pow(9) * 100) {
            achievements[conditions.indexOf(Keys.KEY_ACHIEVEMENTS_3)].achieved = true
        }
        if (initialUserXp + currExp >= 2.0.pow(14) * 100) {
            achievements[conditions.indexOf(Keys.KEY_ACHIEVEMENTS_3)].achieved = true
        }
        if (initialUserXp + currExp >= 2.0.pow(19) * 100) {
            achievements[conditions.indexOf(Keys.KEY_ACHIEVEMENTS_3)].achieved = true
        }
        unsubscribeOnDestroy(
                repository.saveAchievements(achievements)
                        .subscribeWith(object : DisposableCompletableObserver(){
                            override fun onComplete() {
                                Timber.v("Completed saving $achievements to local DB")
                            }

                            override fun onError(e: Throwable) {
                                Timber.e("Got error: ${e.message}")
                            }
                        })
        )
    }

    private fun updateTaskProgressAndXp(){
        val taskId = tasks[taskIndex].task.id
        unsubscribeOnDestroy(
                repository.getProgressByTaskId(taskId)
                        .subscribeWith(object : DisposableSingleObserver<UserProgress>(){
                            override fun onSuccess(t: UserProgress) {
                                t.progressMarker += if (t.progressMarker == 8) 0 else 1
                                if (t.progressMarker == 8) {
                                    repository.updateUserProgress(processProgressUnit(t))
                                } else {
                                    repository.updateUserProgress(t)
                                }
                            }

                            override fun onError(e: Throwable) {
                                val up = UserProgress(id = Random().nextInt(), progressMarker = 1, taskId = taskId)
                                repository.updateUserProgress(up)
                            }
                        })
        )
        unsubscribeOnDestroy(repository.updateUserXp(initialUserXp + currExp))
    }


    private fun processProgressUnit(t: UserProgress): UserProgress {
        when (t.progressDelayMarker) {
            0 -> {
                t.progressDelay = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(1)
                t.progressDelayMarker = 1
            }
            1 -> {
                t.progressDelay = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(2)
                t.progressDelayMarker = 2
            }
            2 -> {
                t.progressDelay = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(4)
                t.progressDelayMarker = 3
            }
            3 -> {
                t.progressDelay = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(7)
                t.progressDelayMarker = 4
            }
            4 -> {
                t.progressDelay = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(14)
                t.progressDelayMarker = 5
            }
            else -> {
                t.progressDelay = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(30)
                t.progressDelayMarker = 5
            }
        }
        return t
    }

    private fun setupNextLessonEntry() {
        taskIndex++
        taskStartTime = System.currentTimeMillis()
        viewState.setNextLessonEntry(tasks[taskIndex])
    }

    private fun getAnswersList(task: Task): List<AnswerEntry> {
        Timber.v("Correct answer: ${task.correctAnswer}")
        Timber.v("Possible answers: ${task.possibleAnswers}")
        val correctAnswer = task.correctAnswer
        val answers = task.possibleAnswers
                .shuffled()
                .subList(0, 3)
                .toMutableList()

        answers.add(correctAnswer)

        Timber.v("Processed answers: $answers")
        return answers.shuffled().map { AnswerEntry(it, it == correctAnswer) }
    }
}