package com.memathteam.memath.mvp.presenters

import android.content.SharedPreferences
import com.arellomobile.mvp.InjectViewState
import com.memathteam.memath.app.App
import com.memathteam.memath.utils.Keys
import com.memathteam.memath.mvp.views.LoginView
import com.memathteam.memath.repository.Repository
import com.memathteam.memath.services.AuthService
import com.memathteam.memath.utils.LoginResponse
import com.memathteam.memath.utils.subscribeIoObserveMain
import com.vk.sdk.VKAccessToken
import com.vk.sdk.api.*
import com.vk.sdk.api.methods.VKApiUsers
import com.vk.sdk.api.model.VKApiUser
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableSingleObserver
import org.json.JSONObject
import timber.log.Timber
import java.util.*
import javax.inject.Inject

/**
 * 28/02/2018 00:23
 * Created by elber.
 */

@InjectViewState
class LoginPresenter : BasePresenter<LoginView>() {

    @Inject
    lateinit var prefs: SharedPreferences

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var authService: AuthService

    init {
        App.appComponent.inject(this)
    }

    fun authenticate(token: VKAccessToken) {
        Timber.v("Token: ${token.accessToken}\nUser: ${token.userId}")
        val reqq = VKApi.users().get(VKParameters.from(VKApiConst.USER_IDS, token.userId))
        reqq.secure = false
        reqq.executeWithListener(object : VKRequest.VKRequestListener(){
            override fun onComplete(response: VKResponse?) {
                Timber.v(response.toString())
                val usr = response?.json?.getJSONArray("response")?.get(0) as JSONObject
                Timber.v(usr.toString())
                val fname = usr.get("first_name") as String
                val sname = usr.get("last_name") as String

                viewState.onLoginStart()
                val req = authService.authorize(token, "$fname $sname")
                        .subscribeIoObserveMain()
                        .subscribeWith(object : DisposableSingleObserver<LoginResponse>(){
                            override fun onSuccess(t: LoginResponse) {
                                Timber.v("Received: $t on ${Thread.currentThread().name}")
                                prefs.edit()
                                        .putString(Keys.APP_PREFERENCES_TOKEN, t.token)
                                        .putString(Keys.VK_USER_ID, token.userId)
                                        .apply()

                                val saveReq = repository.saveUser(t.user)
                                        .subscribeWith(object : DisposableCompletableObserver(){
                                            override fun onComplete() {
                                                Timber.v("Saved ${t.user}")
                                            }

                                            override fun onError(e: Throwable) {
                                                Timber.e(e.message)
                                            }
                                        })

                                unsubscribeOnDestroy(saveReq)

                                Timber.v("Saved token to prefs and ${t.user} to db")
                                viewState.onLoginSuccess()
                            }

                            override fun onError(e: Throwable) {
                                viewState.onLoginFailed()
                                Timber.e(e.message.toString())
                            }
                        })

                unsubscribeOnDestroy(req)
            }
        })

    }
}