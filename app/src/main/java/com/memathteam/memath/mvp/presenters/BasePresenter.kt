package com.memathteam.memath.mvp.presenters

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import timber.log.Timber

/**
 * Created by elber on 27/02/2018.
 */
open class BasePresenter<T : MvpView> : MvpPresenter<T>(){

    private val compositeDisposable = CompositeDisposable()

    fun unsubscribeOnDestroy(disposable: Disposable) {
        Timber.v("Added $disposable")
        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.v("Clearing $compositeDisposable")
        compositeDisposable.clear()
    }
}