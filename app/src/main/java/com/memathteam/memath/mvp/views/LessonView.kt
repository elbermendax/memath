package com.memathteam.memath.mvp.views

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.memathteam.memath.domain.LessonEntry

/**
 * 11/03/2018 18:52
 *
 * @author elber.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface LessonView : MvpView {

    fun setNewXp(xp: Int)

    fun setNextLessonEntry(entry: LessonEntry)

    fun setTimerText(time: Long)

    fun setLessonFinishedLayout(totalXpEarned: Int)

}