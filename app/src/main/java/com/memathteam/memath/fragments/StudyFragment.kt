package com.memathteam.memath.fragments

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.TransitionDrawable
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.memathteam.memath.R
import com.memathteam.memath.activities.LessonActivity
import com.memathteam.memath.adapters.ThemeAdapter
import com.memathteam.memath.data.Category
import com.memathteam.memath.mvp.presenters.StudyPresenter
import com.memathteam.memath.mvp.views.StudyView
import kotlinx.android.synthetic.main.fragment_study.*

/**
 * Created by elber on 18/02/2018.
 */
class StudyFragment : MvpAppCompatFragment(), StudyView {

    @InjectPresenter
    lateinit var studyPresenter: StudyPresenter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_study, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        studyPresenter.attachView(this)
        study_btn_next_lesson.setOnClickListener { studyPresenter.startNextLesson() }
    }

    override fun setupLessonModeChange() {
//        TODO COMPLETE
        study_btn_type_switcher.setOnClickListener {  }
    }

    override fun startNextLesson() {
        val intent = Intent(activity, LessonActivity::class.java)
        startActivity(intent)
    }

    override fun setThemeList(categories: List<Pair<Category, Pair<Int, Int>>>) {
        val llm = LinearLayoutManager(activity)
        llm.reverseLayout = true
        rv_study_items.layoutManager = llm
        rv_study_items.adapter = ThemeAdapter(categories)
    }
}