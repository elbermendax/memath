package com.memathteam.memath.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.memathteam.memath.R
import com.memathteam.memath.activities.LoginActivity
import com.memathteam.memath.adapters.AchievementsAdapter
import com.memathteam.memath.data.Achievement
import com.memathteam.memath.mvp.presenters.UserPresenter
import com.memathteam.memath.mvp.views.UserView
import com.memathteam.memath.utils.Keys
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_user.*
import org.jetbrains.anko.startActivity
import timber.log.Timber
import kotlin.math.pow

/**
 * Created by elber on 18/02/2018.
 */
class UserFragment : MvpAppCompatFragment(), UserView {

    @InjectPresenter
    lateinit var userPresenter: UserPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_user, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        userPresenter.attachView(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.user_menu, menu)
    }

    override fun setAchievements(data: List<Achievement>) {
        rv_achievements.layoutManager = LinearLayoutManager(activity)
        rv_achievements.adapter = AchievementsAdapter(activity, data)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_logout -> {
                userPresenter.logout()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun setName(name: String) {
        tv_user_name.text = name
    }

    override fun setPhoto(imgUrl: String) {
        Picasso.with(activity).load(imgUrl).
                placeholder(R.drawable.ic_photo_camera_black_24dp).into(civ_usr_pic)
    }

    override fun setUserProgressInfo(lvl: Int, xp: Int, nextLevelXp: Int) {
        tv_user_level.text = getString(
                R.string.tv_usr_level_template,
                lvl.toString(), xp.toString(), nextLevelXp.toString()
        )
    }

    override fun setProgress(percentage: Int) {
        Timber.v("Received $percentage percentage to apply")
        pb_user_progress.progress = percentage
    }

    override fun logout() {
        activity.startActivity<LoginActivity>()
        activity.finish()
    }
}