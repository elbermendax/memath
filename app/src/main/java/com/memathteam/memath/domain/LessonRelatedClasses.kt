package com.memathteam.memath.domain

import com.memathteam.memath.data.Task
import com.memathteam.memath.data.UserProgress

/**
 * 11/03/2018 18:53
 *
 * @author elber.
 */
data class LessonEntry(val task: Task, val progress: UserProgress, val answers: List<AnswerEntry>)

data class AnswerEntry(val text: String, val isCorrect: Boolean)