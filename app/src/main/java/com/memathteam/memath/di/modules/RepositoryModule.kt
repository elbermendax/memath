package com.memathteam.memath.di.modules

import com.memathteam.memath.firebase.TaskScheduler
import com.memathteam.memath.repository.LocalRepository
import com.memathteam.memath.repository.RemoteRepository
import com.memathteam.memath.repository.Repository
import com.memathteam.memath.repository.RepositoryImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * 03/03/2018 21:15
 *
 * @author elber.
 */

@Module(includes = [LocalRepositoryModule::class, RemoteRepositoryModule::class, TaskSchedulerModule::class])
class RepositoryModule {

    @Singleton
    @Provides
    fun provideRepository(localRepository: LocalRepository, remoteRepository: RemoteRepository,
                          taskScheduler: TaskScheduler):
            Repository = RepositoryImpl(localRepository, remoteRepository, taskScheduler)

}