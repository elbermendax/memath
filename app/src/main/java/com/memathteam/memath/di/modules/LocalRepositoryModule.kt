package com.memathteam.memath.di.modules

import com.memathteam.memath.data.AppDatabase
import com.memathteam.memath.repository.LocalRepository
import com.memathteam.memath.repository.LocalRepositoryImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * 03/03/2018 21:44
 *
 * @author elber.
 */
@Module
class LocalRepositoryModule {

    @Provides
    @Singleton
    fun provideLocalRepository(db: AppDatabase): LocalRepository = LocalRepositoryImpl(db)
}