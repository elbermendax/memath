package com.memathteam.memath.di.modules

import com.memathteam.memath.app.MemathApi
import com.memathteam.memath.repository.RemoteRepository
import com.memathteam.memath.repository.RemoteRepositoryImpl
import dagger.Lazy
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * 03/03/2018 21:44
 *
 * @author elber.
 */
@Module
class RemoteRepositoryModule {

    @Provides
    @Singleton
    fun provideRemoteRepository(api: MemathApi): RemoteRepository =
            RemoteRepositoryImpl(Lazy { api })
}