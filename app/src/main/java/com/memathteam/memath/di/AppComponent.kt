package com.memathteam.memath.di

import android.support.v7.widget.RecyclerView
import com.memathteam.memath.adapters.ThemeAdapter
import com.memathteam.memath.di.modules.ApiModule
import com.memathteam.memath.di.modules.AppModule
import com.memathteam.memath.di.modules.AuthModule
import com.memathteam.memath.di.modules.RepositoryModule
import com.memathteam.memath.firebase.UpdateJobService
import com.memathteam.memath.mvp.presenters.*
import com.memathteam.memath.repository.Repository
import com.memathteam.memath.services.AuthService
import dagger.Component
import javax.inject.Singleton

/**
 * 03/03/2018 17:26
 * Created by elber.
 */
@Singleton
@Component(modules = [AppModule::class, ApiModule::class, RepositoryModule::class, AuthModule::class])
interface AppComponent {

    fun inject(target: UpdateJobService)
    fun inject(target: LoginPresenter)
    fun inject(target: SplashPresenter)
    fun inject(target: MainPresenter)
    fun inject(target: UserPresenter)
    fun inject(target: StudyPresenter)
    fun inject(target: ThemeAdapter.ThemeViewHolder)
    fun inject(target: LessonPresenter)

}