package com.memathteam.memath.di.modules

import android.content.Context
import com.memathteam.memath.firebase.TaskScheduler
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * 17/03/2018 19:45
 *
 * @author elber.
 */
@Module
class TaskSchedulerModule {

    @Singleton
    @Provides
    fun provideTaskScheduler(context: Context): TaskScheduler = TaskScheduler(context)

}