package com.memathteam.memath.di.modules

import android.content.SharedPreferences
import com.memathteam.memath.app.MemathApi
import com.memathteam.memath.app.MemathAuthApi
import com.memathteam.memath.utils.Keys
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton
import kotlin.math.log

/**
 * 03/03/2018 19:15
 *
 * @author elber.
 */
@Module
class ApiModule {

    @Provides
    @Singleton
    fun provideAuthApi(): MemathAuthApi {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC

        val client = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl(Keys.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build()

        return retrofit.create(MemathAuthApi::class.java)
    }

    @Provides
    fun provideApi(prefs: SharedPreferences): MemathApi {
        val client = OkHttpClient.Builder()

        client.addInterceptor { chain ->
            val original = chain.request()
            val builder = original.newBuilder()
                    .addHeader("Authorization",
                            "Bearer " + prefs.getString(Keys.APP_PREFERENCES_TOKEN, "")
                    )
            val request = builder.build()
            chain.proceed(request)
        }

        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
        client.addInterceptor(loggingInterceptor)

        val retrofit = Retrofit.Builder()
                .baseUrl(Keys.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client.build())
                .build()

        return retrofit.create(MemathApi::class.java)
    }
}