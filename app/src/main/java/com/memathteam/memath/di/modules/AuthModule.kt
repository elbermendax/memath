package com.memathteam.memath.di.modules

import com.memathteam.memath.app.MemathAuthApi
import com.memathteam.memath.services.AuthService
import com.memathteam.memath.services.AuthServiceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * 03/03/2018 21:55
 *
 * @author elber.
 */
@Module
class AuthModule {

    @Provides
    @Singleton
    fun provideAuthService(authApi: MemathAuthApi): AuthService = AuthServiceImpl(authApi)

}