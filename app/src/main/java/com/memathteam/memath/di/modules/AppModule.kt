package com.memathteam.memath.di.modules

import android.arch.persistence.room.Room
import android.content.Context
import android.content.SharedPreferences
import com.memathteam.memath.app.App
import com.memathteam.memath.data.AppDatabase
import com.memathteam.memath.utils.Keys
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * 03/03/2018 20:44
 *
 * @author elber.
 */
@Module
class AppModule(val app: App) {

    @Provides
    @Singleton
    fun provideAppContext(): Context = app

    @Provides
    @Singleton
    fun providePrefs(): SharedPreferences =
            app.getSharedPreferences(Keys.APP_PREFERENCES, Context.MODE_PRIVATE)

    @Provides
    @Singleton
    fun provideDb(): AppDatabase =
            Room.databaseBuilder(app, AppDatabase::class.java, "memath-db").build()

}