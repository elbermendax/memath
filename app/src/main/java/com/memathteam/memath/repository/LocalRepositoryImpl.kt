package com.memathteam.memath.repository

import com.memathteam.memath.data.*
import com.memathteam.memath.utils.subscribeIoObserveMain
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

/**
 * 03/03/2018 19:00
 *
 * @author elber.
 */
class LocalRepositoryImpl(private val db: AppDatabase) : LocalRepository {

    override fun clearUsers(): Completable =
            Completable.fromAction { db.userDao().nukeTable() }

    override fun getAllAchievements(): Single<List<Achievement>> =
            db.achievementsDao().getAllAchievements()

    override fun saveAchievements(achievements: List<Achievement>): Completable =
            Completable.fromAction {
                Timber.v("Saving: $achievements to DB")
                achievements.forEach { db.achievementsDao().insert(it) }
            }

    override fun saveUser(user: User): Completable =
            Completable.fromAction { db.userDao().insert(user) }

    override fun saveCategories(categories: List<Category>): Completable =
            Completable.fromAction { categories.forEach { db.categoryDao().insert(it) } }

    override fun saveProgress(userProgress: List<UserProgress>): Completable =
            Completable.fromAction { userProgress.forEach { db.userProgressDao().insert(it) } }
                    .subscribeOn(Schedulers.io())

    override fun getUser(): Single<User> = db.userDao().getUser()

    override fun getTasksByCategoryId(categoryId: Int): Flowable<List<Task>> =
            db.taskDao().getTasksByCat(categoryId)
                    .doOnNext {
                        Timber.v("Retrieved $it from DB")
                        it.forEach { it.possibleAnswers = it.answersString.split(";") }
                        Timber.v("Updated $it")
                    }

    override fun getProgressByTaskId(taskId: Int): Single<UserProgress> =
            db.userProgressDao().getProgressByTaskId(taskId)

    override fun getAllCategories(): Flowable<List<Category>> =
            db.categoryDao().getAllCategories()

    override fun getCategoryById(categoryId: Int): Flowable<Category> =
            db.categoryDao().getCategoryById(categoryId)

    override fun saveTasks(tasks: List<Task>): Disposable =
            Completable.fromAction {
                tasks.forEach {
                    it.categoryId = it.categoryData.id
                    it.answersString = it.possibleAnswers.joinToString(";")
                    db.taskDao().insert(it)
                }
            }
                    .subscribeIoObserveMain()
                    .subscribeWith(object : DisposableCompletableObserver(){
                        override fun onComplete() {
                            Timber.v("Saved $tasks to DB")
                        }

                        override fun onError(e: Throwable) {
                            Timber.e("Error while saving $tasks to db")
                        }
                    })

    override fun updateUserProgress(userProgress: UserProgress): Completable =
            Completable.fromAction {
                db.userProgressDao().insert(userProgress)
                Timber.v("Updated $userProgress on ${Thread.currentThread().name}")
            }

    override fun getUserProgress(): Flowable<List<UserProgress>> =
            db.userProgressDao().getUserProgress()
}