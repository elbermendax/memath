package com.memathteam.memath.repository

import com.memathteam.memath.app.MemathApi
import com.memathteam.memath.data.Category
import com.memathteam.memath.data.Task
import com.memathteam.memath.data.User
import com.memathteam.memath.data.UserProgress
import com.memathteam.memath.utils.XpUpdate
import dagger.Lazy
import io.reactivex.Single

/**
 * 03/03/2018 19:00
 *
 * @author elber.
 */
class RemoteRepositoryImpl(private val api: Lazy<MemathApi>) : RemoteRepository {

    override fun getTasksByCategory(categoryId: Int): Single<List<Task>> =
            api.get().getTasksByCategory(categoryId)

    override fun getFormulasByCategory(categoryId: Int): Single<List<Task>> =
            api.get().getFormulasByCategory(categoryId)

    override fun getUser(): Single<User> = api.get().getUser()

    override fun updateUserXp(totalXp: XpUpdate): Single<User> = api.get().updateUserXp(totalXp)

    override fun updateUserProgress(progress: UserProgress): Single<UserProgress> =
            api.get().updateUserProgress(progress)

    override fun getUserProgress(): Single<List<UserProgress>> = api.get().getUserProgress()

    override fun getAllCategories(): Single<List<Category>> = api.get().getAllCategories()

}