package com.memathteam.memath.repository

import com.memathteam.memath.data.Category
import com.memathteam.memath.data.Task
import com.memathteam.memath.data.User
import com.memathteam.memath.data.UserProgress
import com.memathteam.memath.utils.XpUpdate
import io.reactivex.Single

/**
 * 03/03/2018 18:59
 *
 * @author elber.
 */
interface RemoteRepository {

    fun getTasksByCategory(categoryId: Int): Single<List<Task>>

    fun getFormulasByCategory(categoryId: Int): Single<List<Task>>

    fun getUser(): Single<User>

    fun updateUserXp(totalXp: XpUpdate): Single<User>

    fun updateUserProgress(progress: UserProgress): Single<UserProgress>

    fun getUserProgress(): Single<List<UserProgress>>

    fun getAllCategories(): Single<List<Category>>
}