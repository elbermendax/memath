package com.memathteam.memath.repository

import com.memathteam.memath.data.*
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.disposables.Disposable

/**
 * 03/03/2018 18:59
 *
 * @author elber.
 */
interface LocalRepository {

    fun getAllAchievements(): Single<List<Achievement>>

    fun clearUsers(): Completable

    fun saveAchievements(achievements: List<Achievement>): Completable

    fun saveUser(user: User): Completable

    fun saveCategories(categories: List<Category>): Completable

    fun saveTasks(tasks: List<Task>): Disposable

    fun saveProgress(userProgress: List<UserProgress>): Completable

    fun updateUserProgress(userProgress: UserProgress): Completable

    fun getUser(): Single<User>

    fun getUserProgress(): Flowable<List<UserProgress>>

    fun getTasksByCategoryId(categoryId: Int): Flowable<List<Task>>

    fun getProgressByTaskId(taskId: Int): Single<UserProgress>

    fun getAllCategories(): Flowable<List<Category>>

    fun getCategoryById(categoryId: Int): Flowable<Category>

}