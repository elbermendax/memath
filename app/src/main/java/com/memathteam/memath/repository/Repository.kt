package com.memathteam.memath.repository

import com.memathteam.memath.data.*
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.disposables.Disposable

/**
 * 03/03/2018 21:49
 *
 * @author elber.
 */
interface Repository {

    fun setupAchievements(): Disposable

    fun clearUsers(): Completable

    fun getAchievements(): Single<List<Achievement>>

    fun saveAchievements(achievements: List<Achievement>): Completable

    fun saveUser(user: User): Completable

    fun cacheCategories(): Disposable

    fun cacheTasks(cats: List<Category>): Disposable

    fun cacheProgress(): Disposable

    fun getCategories(): Flowable<List<Category>>

    fun getCategoryById(categoryId: Int): Flowable<Category>

    fun getProgressByTaskId(taskId: Int): Single<UserProgress>

    fun getUserProgress(): Flowable<List<UserProgress>>

    fun getUser(): Single<User>

    fun getTasksByCategory(categoryId: Int): Flowable<List<Task>>

    fun updateUserProgress(userProgress: UserProgress): Disposable

    fun updateUserXp(totalXp: Int): Disposable

    fun getProgressForCategory(categoryId: Int): Flowable<Pair<Int, Int>>

}