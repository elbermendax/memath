package com.memathteam.memath.repository

import com.memathteam.memath.data.*
import com.memathteam.memath.firebase.TaskScheduler
import com.memathteam.memath.utils.JobType
import com.memathteam.memath.utils.Keys
import com.memathteam.memath.utils.XpUpdate
import com.memathteam.memath.utils.subscribeIoObserveMain
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

/**
 * 03/03/2018 18:59
 *
 * @author elber.
 */
class RepositoryImpl(
        private val local: LocalRepository,
        private val remote: RemoteRepository,
        private val taskScheduler: TaskScheduler
): Repository {

    override fun clearUsers(): Completable = local.clearUsers().subscribeIoObserveMain()

    override fun getAchievements(): Single<List<Achievement>> =
            local.getAllAchievements().subscribeIoObserveMain()

    override fun saveAchievements(achievements: List<Achievement>): Completable =
            local.saveAchievements(achievements).subscribeIoObserveMain()

    override fun setupAchievements(): Disposable =
            local.getAllAchievements()
                    .subscribeIoObserveMain()
                    .subscribeWith(object : DisposableSingleObserver<List<Achievement>>(){
                        override fun onSuccess(t: List<Achievement>) {
                            Timber.v("Received $t")
                            if (t.isEmpty()) {
                                local.saveAchievements(
                                        listOf(
                                                Achievement(condition = Keys.KEY_ACHIEVEMENTS_1),
                                                Achievement(condition = Keys.KEY_ACHIEVEMENTS_2),
                                                Achievement(condition = Keys.KEY_ACHIEVEMENTS_3),
                                                Achievement(condition = Keys.KEY_ACHIEVEMENTS_4),
                                                Achievement(condition = Keys.KEY_ACHIEVEMENTS_5)
                                        )
                                )
                                        .subscribeIoObserveMain()
                                        .subscribeWith(object : DisposableCompletableObserver(){
                                            override fun onComplete() { dispose() }

                                            override fun onError(e: Throwable) { dispose() }
                                        })
                            }
                        }

                        override fun onError(e: Throwable) {
                            Timber.e("Got error: ${e.message}")
                        }
                    })


    override fun saveUser(user: User): Completable = local.saveUser(user).subscribeIoObserveMain()

    override fun getCategories(): Flowable<List<Category>> =
            local.getAllCategories().subscribeIoObserveMain()

    override fun cacheCategories(): Disposable =
            remote.getAllCategories()
                    .subscribeIoObserveMain()
                    .subscribeWith(object : DisposableSingleObserver<List<Category>>(){
                        override fun onSuccess(t: List<Category>) {
                            Timber.v("Received categories: $t")
                            local.saveCategories(t)
                                    .subscribeIoObserveMain()
                                    .subscribeWith(object : DisposableCompletableObserver() {
                                        override fun onComplete() {
                                            Timber.v("Saved categories to local DB")
                                            cacheTasks(t)
                                        }

                                        override fun onError(e: Throwable) {
                                            Timber.e("Error while saving categories: ${e.message}")
                                        }
                                    })
                        }

                        override fun onError(e: Throwable) {
                            Timber.e("Error while loading categories: ${e.message}")
                        }
                    })

    override fun cacheTasks(cats: List<Category>): Disposable {
        val res = CompositeDisposable()
        cats.forEach {
            res.add(
                    remote.getFormulasByCategory(it.id)
                            .subscribeIoObserveMain()
                            .subscribeWith(object : DisposableSingleObserver<List<Task>>() {
                                override fun onSuccess(t: List<Task>) {
                                    Timber.v("Received formulas: $t")
                                    local.saveTasks(t)
                                }

                                override fun onError(e: Throwable) {
                                    Timber.e("Error while loading formulas: ${e.message}")
                                }
                            })
            )
            res.add(
                    remote.getTasksByCategory(it.id)
                            .subscribeIoObserveMain()
                            .subscribeWith(object : DisposableSingleObserver<List<Task>>() {
                                override fun onSuccess(t: List<Task>) {
                                    Timber.v("Received tasks: $t")
                                    local.saveTasks(t)
                                }

                                override fun onError(e: Throwable) {
                                    Timber.e("Error while loading tasks: ${e.message}")
                                }
                            })
            )
        }
        return res
    }

    override fun cacheProgress(): Disposable =
            remote.getUserProgress()
                    .subscribeIoObserveMain()
                    .subscribeWith(object : DisposableSingleObserver<List<UserProgress>>() {
                        override fun onSuccess(t: List<UserProgress>) {
                            Timber.v("Received user progress: $t")
                            local.saveProgress(t)
                                    .subscribeIoObserveMain()
                                    .subscribeWith(object : DisposableCompletableObserver(){
                                        override fun onComplete() {
                                            Timber.v("Saved user progress from net to DB")
                                        }

                                        override fun onError(e: Throwable) {
                                            Timber.e("Error while saving user progress from web to DB: ${e.message}")
                                        }
                                    })
                        }

                        override fun onError(e: Throwable) {
                            Timber.e("Error while loading user progress: ${e.message}")
                        }
                    })

    override fun getCategoryById(categoryId: Int): Flowable<Category> =
            local.getCategoryById(categoryId).subscribeIoObserveMain()

    override fun getProgressByTaskId(taskId: Int): Single<UserProgress> =
            local.getProgressByTaskId(taskId).subscribeIoObserveMain()

    override fun getUserProgress(): Flowable<List<UserProgress>> =
            local.getUserProgress().subscribeIoObserveMain()

    override fun getUser(): Single<User> = local.getUser().subscribeIoObserveMain()

    override fun getTasksByCategory(categoryId: Int): Flowable<List<Task>> =
            local.getTasksByCategoryId(categoryId).subscribeIoObserveMain()


    override fun updateUserProgress(userProgress: UserProgress): Disposable =
            local.updateUserProgress(userProgress).subscribeIoObserveMain()
                    .subscribeWith(object : DisposableCompletableObserver(){
                        override fun onComplete() {
                            Timber.v("Saved $userProgress locally")
                            remote.updateUserProgress(userProgress)
                                    .subscribeIoObserveMain()
                                    .subscribeWith(object : DisposableSingleObserver<UserProgress>(){
                                        override fun onSuccess(t: UserProgress) {
                                            Timber.v("Updated $userProgress on remote to be: $t")
                                        }

                                        override fun onError(e: Throwable) {
                                            Timber.e("Error while saving $userProgress on remote")
                                            taskScheduler.scheduleJob(JobType.UpdateProgress, userProgress.taskId)
                                        }
                                    })
                        }

                        override fun onError(e: Throwable) {
                            Timber.e("Error while saving $userProgress on local DB")
                        }
                    })


    override fun updateUserXp(totalXp: Int): Disposable =
            local.getUser().subscribeIoObserveMain()
                    .subscribeWith(object : DisposableSingleObserver<User>(){
                        override fun onSuccess(t: User) {
                            t.totalXp = totalXp
                            local.saveUser(t).subscribeIoObserveMain()
                                    .subscribeWith(object : DisposableCompletableObserver(){
                                        override fun onComplete() {
                                            Timber.v("Saved new xp on DB")
                                            remote.updateUserXp(XpUpdate(totalXp))
                                                    .subscribeIoObserveMain()
                                                    .subscribeWith(object : DisposableSingleObserver<User>(){
                                                        override fun onSuccess(t: User) {
                                                            Timber.v("Updated totalXp to be = $totalXp for $t on remote")
                                                        }

                                                        override fun onError(e: Throwable) {
                                                            taskScheduler.clearPreviousXpUpdate()
                                                            taskScheduler.scheduleJob(JobType.UpdateXp, totalXp)
                                                        }
                                                    })
                                        }

                                        override fun onError(e: Throwable) {
                                            Timber.e("Error saving user on DB: ${e.message}")
                                        }
                                    })
                        }

                        override fun onError(e: Throwable) {
                            Timber.e("Got error: ${e.message}")
                        }
                    })

    override fun getProgressForCategory(categoryId: Int): Flowable<Pair<Int, Int>> =
        local.getTasksByCategoryId(categoryId)
                .subscribeOn(Schedulers.io())
                .flatMap {
                    Timber.v("Started getting progress:\n$it")
                    val tmpPairs = it.map { Pair(it.id, it.type) }
                    Timber.v("tmpPairs: $tmpPairs")
                    local.getUserProgress()
                            .subscribeIoObserveMain()
                            .map {
                                Timber.v("Filtering $it")
                                it.filter { it.taskId in tmpPairs.map { it.first } }
                            }
                            .map {
                                Timber.v("Filtered result: $it")
                                var counterTasks = 0
                                var counterFormulas = 0

                                val map = tmpPairs.toMap()
                                Timber.v("Mapping pairs: $tmpPairs to $map")

                                val amountOfFormulas = tmpPairs.count { it.second == 2 }
                                val amountOfTasks = tmpPairs.count { it.second == 1 }
                                Timber.v("Amount of formulas/tasks: $amountOfFormulas/$amountOfTasks")

                                it.forEach {
                                    if (it.progressMarker == 8 && map[it.taskId] == 1) counterTasks += 1
                                    if (it.progressMarker == 8 && map[it.taskId] == 2) counterFormulas += 1
                                }

                                Timber.v("$counterTasks $counterFormulas")

                                if (tmpPairs.isNotEmpty()) {
                                    Pair(
                                            (counterFormulas / amountOfFormulas.toFloat() * 100).toInt(),
                                            (counterTasks / amountOfTasks.toFloat() * 100).toInt()
                                    )
                                } else {
                                    Pair(0, 0)
                                }
                            }
                }
                .subscribeIoObserveMain()
}