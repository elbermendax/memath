package com.memathteam.memath.app

import com.memathteam.memath.data.Category
import com.memathteam.memath.data.Task
import com.memathteam.memath.data.User
import com.memathteam.memath.data.UserProgress
import com.memathteam.memath.utils.XpUpdate
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * 03/03/2018 19:25
 *
 * @author elber.
 */
interface MemathApi {

    @GET("tasks")
    fun getTasksByCategory(@Query("category_id") catId: Int): Single<List<Task>>

    @GET("formulas")
    fun getFormulasByCategory(@Query("category_id") catId: Int): Single<List<Task>>

    @GET("user")
    fun getUser(): Single<User>

    @POST("user")
    fun updateUserXp(@Body xpUpdate: XpUpdate): Single<User>

    @POST("progress")
    fun updateUserProgress(@Body body: UserProgress): Single<UserProgress>

    @GET("progress")
    fun getUserProgress(): Single<List<UserProgress>>

    @GET("categories")
    fun getAllCategories(): Single<List<Category>>

}