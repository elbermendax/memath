package com.memathteam.memath.app

import com.memathteam.memath.utils.LoginRequest
import com.memathteam.memath.utils.LoginResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * 03/03/2018 19:25
 *
 * @author elber.
 */
interface MemathAuthApi {

    @POST("social/login")
    fun authorize(@Body loginRequest: LoginRequest): Single<LoginResponse>

}