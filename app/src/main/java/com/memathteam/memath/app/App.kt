package com.memathteam.memath.app

import android.app.Application
import android.content.Context
import com.memathteam.memath.di.AppComponent
import com.memathteam.memath.di.DaggerAppComponent
import com.memathteam.memath.di.modules.AppModule
import com.memathteam.memath.utils.Keys
import com.vk.sdk.VKSdk
import timber.log.Timber

/**
 * Created by elber on 17/02/2018.
 */
class App : Application() {
    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        VKSdk.initialize(this)

        Timber.plant(Timber.DebugTree())

        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
    }
}