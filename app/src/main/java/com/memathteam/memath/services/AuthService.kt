package com.memathteam.memath.services

import com.memathteam.memath.utils.LoginResponse
import com.vk.sdk.VKAccessToken
import io.reactivex.Single

/**
 * 03/03/2018 21:53
 *
 * @author elber.
 */
interface AuthService {

    fun authorize(vkToken: VKAccessToken, name: String): Single<LoginResponse>

}