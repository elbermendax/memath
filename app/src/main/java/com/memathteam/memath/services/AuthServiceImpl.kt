package com.memathteam.memath.services

import com.memathteam.memath.app.MemathAuthApi
import com.memathteam.memath.utils.LoginRequest
import com.memathteam.memath.utils.LoginResponse
import com.vk.sdk.VKAccessToken
import io.reactivex.Single

/**
 * 03/03/2018 19:27
 *
 * @author elber.
 */
class AuthServiceImpl(val authApi: MemathAuthApi) : AuthService{

    override fun authorize(vkToken: VKAccessToken, name: String): Single<LoginResponse> =
            authApi.authorize(LoginRequest(access_token = vkToken.accessToken, name = name))

}