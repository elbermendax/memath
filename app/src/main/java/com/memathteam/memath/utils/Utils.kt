package com.memathteam.memath.utils

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.*

/**
 * 28/02/2018 00:46
 * Created by elber.
 */

fun <T> Observable<T>.subscribeIoObserveMain(): Observable<T> =
        this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

fun <T> Single<T>.subscribeIoObserveMain(): Single<T> =
        this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

fun <T> Maybe<T>.subscribeIoObserveMain(): Maybe<T> =
        this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

fun <T> Flowable<T>.subscribeIoObserveMain(): Flowable<T> =
        this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

fun Completable.subscribeIoObserveMain(): Completable =
        this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

fun ViewGroup.inflateWith(resId: Int): View =
        LayoutInflater.from(this.context).inflate(resId, this, false)
