package com.memathteam.memath.utils

/**
 * 17/03/2018 19:33
 *
 * @author elber.
 */
enum class JobType {
    UpdateXp,
    UpdateProgress
}