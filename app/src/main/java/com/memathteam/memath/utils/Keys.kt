package com.memathteam.memath.utils

/**
 * Created by elber on 17/02/2018.
 */
class Keys {
    companion object {
        const val APP_PREFERENCES = "mysettings"
        const val APP_PREFERENCES_TOKEN = "access_token"
        const val VK_USER_ID = "vk_user_id"
        const val VK_USER_PHOTO_URL = "vk_user_photo_url"
        const val BASE_URL = "http://memath.benyazi.ru/api/"

        const val KEY_JOB_XP = "update_xp_service_job_tag"
        const val KEY_JOB_PROGRESS_ID  = "update_progress_service_job_tag"

        const val KEY_ACHIEVEMENTS_1 = "xp;800"
        const val KEY_ACHIEVEMENTS_2 = "xp;1600"
        const val KEY_ACHIEVEMENTS_3 = "lvl;10"
        const val KEY_ACHIEVEMENTS_4 = "lvl;15"
        const val KEY_ACHIEVEMENTS_5 = "lvl;20"
    }
}