package com.memathteam.memath.utils

import com.memathteam.memath.data.User

/**
 * Created by elber on 17/02/2018.
 */
data class LoginRequest(val provider: String = "vkontakte",
                        val access_token: String,
                        val name: String)

data class LoginResponse(val user: User, val token: String)

data class XpUpdate(val totalxp: Int)