package com.memathteam.memath.firebase

import android.content.Context
import android.os.Bundle
import com.firebase.jobdispatcher.*
import com.memathteam.memath.utils.JobType
import com.memathteam.memath.utils.Keys
import timber.log.Timber
import java.util.*


/**
 * Created by Александр on 09.03.2018.
 */
class TaskScheduler(private val context: Context) {

    private val dispatcher by lazy { FirebaseJobDispatcher(GooglePlayDriver(context)) }

    /**
     * @param jobType defines which kind of job to schedule
     * @param param in case of JobType.UpdateXp it should be new xp value, in case of JobType.UpdateProgress it should be taskId
     */
    fun scheduleJob(jobType: JobType, param: Int){
        val bundle = Bundle()
        when(jobType){
            JobType.UpdateXp -> bundle.putInt(Keys.KEY_JOB_XP, param)
            else -> bundle.putInt(Keys.KEY_JOB_PROGRESS_ID, param)
        }

        val job = dispatcher.newJobBuilder()
                .setService(UpdateJobService::class.java)
                .setTag(if (jobType == JobType.UpdateXp) Keys.KEY_JOB_XP else UUID.randomUUID().toString())
                .setExtras(bundle)
                .setLifetime(Lifetime.FOREVER)
                .setTrigger(Trigger.executionWindow(5, 10))
                .setReplaceCurrent(true)
                .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .build()

        dispatcher.mustSchedule(job)
    }

    fun clearPreviousXpUpdate() {
        dispatcher.cancel(Keys.KEY_JOB_XP)
    }

}