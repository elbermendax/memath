package com.memathteam.memath.firebase

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.firebase.jobdispatcher.JobParameters
import com.firebase.jobdispatcher.JobService
import com.memathteam.memath.app.App
import com.memathteam.memath.data.UserProgress
import com.memathteam.memath.repository.Repository
import com.memathteam.memath.utils.Keys
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Александр on 09.03.2018.
 */
class UpdateJobService : JobService() {

    @Inject
    lateinit var repository: Repository
    @Inject
    lateinit var context: Context

    private lateinit var connectivityManager: ConnectivityManager
    private lateinit var activeNetworkInfo: NetworkInfo
    private var disposablesMap = mutableMapOf<String, Disposable>()

    init {
        App.appComponent.inject(this)
    }

    override fun onStartJob(job: JobParameters): Boolean {
        connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        activeNetworkInfo = connectivityManager.activeNetworkInfo

        Timber.v("onStartJob: ${job.tag}")

        val extras = job.extras
        if (job.tag == Keys.KEY_JOB_XP) {
            if (extras != null)
                Thread(Runnable { updateXp(extras.getInt(Keys.KEY_JOB_XP), job) }).start()
        } else {
            if (extras != null)
                Thread(Runnable { updateProgress(extras.getInt(Keys.KEY_JOB_PROGRESS_ID), job) }).start()
        }

        return true

    }

    override fun onStopJob(job: JobParameters): Boolean {
        disposablesMap[job.tag]?.dispose()
        disposablesMap.remove(job.tag)
        return false
    }

    private fun updateXp(xp: Int, job: JobParameters) {
        if (activeNetworkInfo.isConnected) {
            disposablesMap[job.tag] = repository.updateUserXp(xp)
        } else {
            Timber.v("Restarting job: ${job.tag}")
            jobFinished(job, true)
        }
    }

    private fun updateProgress(id: Int, job: JobParameters) {
        if (activeNetworkInfo.isConnected) {
            repository.getProgressByTaskId(id)
                    .subscribeWith(object : DisposableSingleObserver<UserProgress>() {
                        override fun onSuccess(t: UserProgress) {
                            disposablesMap[job.tag] = repository.updateUserProgress(t)
                            dispose()
                        }

                        override fun onError(e: Throwable) {
                            dispose()
                        }
                    })
        } else {
            Timber.v("Restarting job: ${job.tag}")
            jobFinished(job, true)
        }
    }
}