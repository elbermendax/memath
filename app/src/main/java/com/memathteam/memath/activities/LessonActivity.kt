package com.memathteam.memath.activities

import android.content.res.ColorStateList
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.memathteam.memath.adapters.AnswersAdapter
import com.memathteam.memath.R
import com.memathteam.memath.domain.AnswerEntry
import com.memathteam.memath.domain.LessonEntry
import com.memathteam.memath.mvp.presenters.LessonPresenter
import com.memathteam.memath.mvp.views.LessonView
import kotlinx.android.synthetic.main.activity_lesson.*
import kotlinx.android.synthetic.main.answer_item.view.*
import timber.log.Timber

class LessonActivity : MvpAppCompatActivity(), AnswersAdapter.AnswerOnClickListener, LessonView {

    @InjectPresenter
    lateinit var presenter: LessonPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lesson)

        rv_lesson_answers.layoutManager = LinearLayoutManager(this)

        presenter.startLesson()

        btn_got_it.setOnClickListener { presenter.gotItWorkout() }
    }

    override fun setNewXp(xp: Int) {
        tv_exp_meter.animateText("$xp XP")
    }

    override fun setTimerText(time: Long) {
        tv_timer_countdown.text = String.format("%02d:%02d", time / 60000, time / 1000)
    }

    override fun setLessonFinishedLayout(totalXpEarned: Int) {
        lesson_ll_holder.visibility = View.GONE
        lesson_finished_holder.visibility = View.VISIBLE
        tv_lesson_total_xp.text = "$totalXpEarned XP"
        btn_finish_lesson.setOnClickListener { finish() }
    }

    override fun setNextLessonEntry(entry: LessonEntry) {
        if (entry.answers.isEmpty()) {
            task_placeholder.setDisplayText("$" + entry.task.task + "=$\n$" + entry.task.correctAnswer + "$")
            rv_lesson_answers.visibility = View.GONE
            ll_learn_again.visibility = View.VISIBLE
        } else {
            if (entry.task.type == 2) {
                task_placeholder.setDisplayText("$" + entry.task.task + "=?$")
            } else {
                entry.task.task = entry.task.task.replace("""\\""", "$\n$")
                task_placeholder.setDisplayText("$" + entry.task.task + "$")
            }
            ll_learn_again.visibility = View.GONE
            rv_lesson_answers.visibility = View.VISIBLE
            Timber.v("$entry")
            rv_lesson_answers.adapter = AnswersAdapter(entry.answers, this)
        }
    }

    override fun onClick(elem: AnswerEntry, view: View) {
        if (elem.isCorrect) {
            view.cv_answer_placeholder.cardBackgroundColor =
                    ColorStateList.valueOf(ContextCompat.getColor(this, android.R.color.holo_green_light))
            presenter.processCorrectAnswer()
        } else {
            view.cv_answer_placeholder.cardBackgroundColor =
                    ColorStateList.valueOf(ContextCompat.getColor(this, android.R.color.holo_red_light))
            presenter.processWrongAnswer()
        }
    }
}
