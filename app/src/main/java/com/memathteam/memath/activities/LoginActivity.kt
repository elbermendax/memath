package com.memathteam.memath.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.memathteam.memath.R
import com.memathteam.memath.mvp.presenters.LoginPresenter
import com.memathteam.memath.mvp.views.LoginView
import com.vk.sdk.VKAccessToken
import com.vk.sdk.VKCallback
import com.vk.sdk.VKSdk
import com.vk.sdk.api.VKError
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import timber.log.Timber

class LoginActivity : MvpAppCompatActivity(), LoginView {

    @InjectPresenter
    lateinit var presenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setSupportActionBar(null)

        btn_vk_login.setOnClickListener { vkSdkLogin() }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, object : VKCallback<VKAccessToken> {
                    override fun onResult(res: VKAccessToken?) {
                        if (res != null) authorizeAtServer(res)
                    }

                    override fun onError(error: VKError?) {
                        Timber.e("User hasn't authorized: ${error?.errorMessage}")
                    }

                })
        ){
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun vkSdkLogin() {
        VKSdk.login(this)
    }

    override fun authorizeAtServer(token: VKAccessToken) {
        presenter.authenticate(token)
    }

    override fun onLoginSuccess() {
        hideProgress()
        login_success.visibility = View.VISIBLE
        Handler().postDelayed({ navigateToMainActivity() }, 800)
    }

    override fun onLoginFailed() {
        toast("There's some problem, try again later")
        onLoginFinished()
    }

    override fun showProgress() {
        login_pb_loading.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        login_pb_loading.visibility = View.GONE
    }

    override fun onLoginStart() {
        showProgress()
        btn_vk_login.visibility = View.GONE
    }

    override fun onLoginFinished() {
        hideProgress()
        btn_vk_login.visibility = View.VISIBLE
    }

    override fun navigateToMainActivity() {
        startActivity<MainActivity>()
        finish()
    }
}
