package com.memathteam.memath.activities

import android.os.Bundle
import android.view.MenuItem
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.memathteam.memath.R
import com.memathteam.memath.fragments.ProFragment
import com.memathteam.memath.fragments.StudyFragment
import com.memathteam.memath.fragments.UserFragment
import com.memathteam.memath.mvp.presenters.MainPresenter
import com.memathteam.memath.mvp.views.MainView
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity

class MainActivity : MvpAppCompatActivity(), MainView{

    @InjectPresenter
    lateinit var mainPresenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainPresenter.attachView(this)

        mainPresenter.cacheData()

        bnv_controls.selectedItemId = R.id.action_user
    }

    override fun setupNavigation() {
        bnv_controls.setOnNavigationItemSelectedListener { item: MenuItem ->
            when(item.itemId){
                R.id.action_user -> {
                    inflateFragment(UserFragment())
                    true
                }
                R.id.action_study -> {
                    inflateFragment(StudyFragment())
                    true
                }
                R.id.action_pro -> {
                    inflateFragment(ProFragment())
                    true
                }
                else -> true
            }
        }
    }

    override fun inflateFragment(fragment: MvpAppCompatFragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit()
    }
}
