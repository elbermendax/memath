package com.memathteam.memath.activities

import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.memathteam.memath.R
import com.memathteam.memath.mvp.presenters.SplashPresenter
import com.memathteam.memath.mvp.views.SplashView
import org.jetbrains.anko.startActivity


class SplashActivity : MvpAppCompatActivity(), SplashView {

    @InjectPresenter
    lateinit var splashPresenter: SplashPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        setSupportActionBar(null)

        splashPresenter.attachView(this)
    }

    override fun navigateToLogin() {
        startActivity<LoginActivity>()
        finish()
    }

    override fun navigateToMainScreen() {
        startActivity<MainActivity>()
        finish()
    }
}
