package com.memathteam.memath.data

import android.arch.persistence.room.*
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface UserDao{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: User)

    @Delete
    fun delete(user: User)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(user: User)

    @Query("SELECT * FROM users LIMIT 1")
    fun getUser(): Single<User>

    @Query("DELETE FROM users")
    fun nukeTable()
}

@Dao
interface TaskDao{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(task: Task)

    @Delete
    fun delete(task: Task)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(task: Task)

    @Query("SELECT * FROM tasks WHERE categoryId = :catId")
    fun getTasksByCat(catId: Int): Flowable<List<Task>>

    @Query("DELETE FROM tasks")
    fun nukeTable()
}

@Dao
interface AchievementsDao{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(achievement: Achievement)

    @Delete
    fun delete(achievement: Achievement)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(achievement: Achievement)

    @Query("SELECT * FROM achievements")
    fun getAllAchievements(): Single<List<Achievement>>

    @Query("DELETE FROM achievements")
    fun nukeTable()
}

@Dao
interface UserProgressDao{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(progress: UserProgress)

    @Delete
    fun delete(progress: UserProgress)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(progress: UserProgress)

    @Query("SELECT * FROM user_progress")
    fun getUserProgress(): Flowable<List<UserProgress>>

    @Query("SELECT * FROM user_progress WHERE taskId = :taskId")
    fun getProgressByTaskId(taskId: Int): Single<UserProgress>

    @Query("DELETE FROM tasks")
    fun nukeTable()
}

@Dao
interface CategoryDao{

    @Query("SELECT * FROM categories WHERE id = :id")
    fun getCategoryById(id: Int): Flowable<Category>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(category: Category)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(category: Category)

    @Delete
    fun delete(category: Category)

    @Query("SELECT * FROM categories")
    fun getAllCategories(): Flowable<List<Category>>

    @Query("DELETE FROM categories")
    fun nukeTable()
}