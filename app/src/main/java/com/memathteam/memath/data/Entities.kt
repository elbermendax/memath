package com.memathteam.memath.data

import android.arch.persistence.room.*
import com.google.gson.annotations.SerializedName

/**
 * Created by elber on 17/02/2018.
 */
@Entity(tableName = "users")
data class User(@PrimaryKey var id: Int = 0,
                var name: String = "",
                @SerializedName("totalxp") var totalXp: Int = 0)

@Entity(tableName = "user_progress")
data class UserProgress(@PrimaryKey var id: Int = 0,
                        @SerializedName("user_id") var userId: Int = 0,
                        @SerializedName("task_id") var taskId: Int = 0,
                        @SerializedName("progress_marker") var progressMarker: Int = 0,
                        @SerializedName("progress_delay")var progressDelay: Long = 0,
                        @SerializedName("progress_delay_marker") var progressDelayMarker: Int = 0)

@Entity(tableName = "tasks")
data class Task(@PrimaryKey var id: Int = 0,
                var categoryId: Int = 0,
                @Ignore var categoryData: Category = Category(),
                var task: String = "",
                @SerializedName("possible_answers") @Ignore var possibleAnswers: List<String> = listOf(),
                var answersString: String = "",
                var solution: String? = null,
                var type: Int = 0,
                @SerializedName("correct_answer") var correctAnswer: String = "")

@Entity(tableName = "achievements")
data class Achievement(@PrimaryKey(autoGenerate = true) var id: Int = 0,
                       var condition: String = "",
                       var achieved: Boolean = false)

@Entity(tableName = "categories")
data class Category(@PrimaryKey var id: Int = 0,
                    var name: String = "")