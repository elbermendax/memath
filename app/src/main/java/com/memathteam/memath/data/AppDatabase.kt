package com.memathteam.memath.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters

/**
 * Created by elber on 17/02/2018.
 */
@Database(entities = [User::class, Task::class, UserProgress::class, Category::class, Achievement::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao() : UserDao
    abstract fun taskDao() : TaskDao
    abstract fun userProgressDao() : UserProgressDao
    abstract fun categoryDao() : CategoryDao
    abstract fun achievementsDao() : AchievementsDao
}