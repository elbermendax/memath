package com.memathteam.memath.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.memathteam.memath.R
import com.memathteam.memath.domain.AnswerEntry
import com.memathteam.memath.utils.inflateWith
import kotlinx.android.synthetic.main.answer_item.view.*

/**
 * Created by elber on 18/02/2018.
 */
class AnswersAdapter(val data: List<AnswerEntry>,
                     private val onClickListener: AnswerOnClickListener) : RecyclerView.Adapter<AnswersAdapter.AnswerViewHolder>() {

    interface AnswerOnClickListener{
        fun onClick(elem: AnswerEntry, view: View)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnswerViewHolder {
        return AnswerViewHolder(
                parent.inflateWith(R.layout.answer_item),
                onClickListener
        )
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: AnswerViewHolder?, position: Int) {
        holder?.bind(data[position])
    }

    class AnswerViewHolder(view: View,
                           private val onClickListener: AnswerOnClickListener) : RecyclerView.ViewHolder(view) {

        fun bind(elem: AnswerEntry){
            itemView.tv_answer_item_btn.setOnClickListener { onClickListener.onClick(elem, itemView) }
            itemView.mv_answer_item.setDisplayText("\$${elem.text}\$")
        }

    }
}