package com.memathteam.memath.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.memathteam.memath.R
import com.memathteam.memath.data.Achievement
import com.memathteam.memath.utils.Keys
import com.memathteam.memath.utils.inflateWith
import kotlinx.android.synthetic.main.item_achievement.view.*

/**
 * 17/03/2018 21:24
 *
 * @author elber.
 */
class AchievementsAdapter(
        private val context: Context,
        private val data: List<Achievement>
) : RecyclerView.Adapter<AchievementsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(context, parent.inflateWith(R.layout.item_achievement))

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.bind(data[position])
    }

    class ViewHolder(private val context: Context, view: View) : RecyclerView.ViewHolder(view) {
        fun bind(elem: Achievement){
            itemView.achievement_description.text = when (elem.condition) {
                Keys.KEY_ACHIEVEMENTS_1 -> context.getString(R.string.xp_800)
                Keys.KEY_ACHIEVEMENTS_2 -> context.getString(R.string.xp_1600)
                Keys.KEY_ACHIEVEMENTS_3 -> context.getString(R.string.lvl_10)
                Keys.KEY_ACHIEVEMENTS_4 -> context.getString(R.string.lvl_15)
                else -> context.getString(R.string.lvl_20)
            }
        }
    }

}