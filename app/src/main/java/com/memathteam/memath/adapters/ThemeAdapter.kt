package com.memathteam.memath.adapters

import android.content.Context
import android.content.res.Resources
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.memathteam.memath.R
import com.memathteam.memath.app.App
import com.memathteam.memath.data.Category
import com.memathteam.memath.utils.inflateWith
import kotlinx.android.synthetic.main.theme_item.view.*
import javax.inject.Inject

/**
 * Created by elber on 18/02/2018.
 */
class ThemeAdapter(val data: List<Pair<Category, Pair<Int, Int>>>) :
        RecyclerView.Adapter<ThemeAdapter.ThemeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThemeViewHolder =
            ThemeViewHolder(parent.inflateWith(R.layout.theme_item))

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ThemeViewHolder?, position: Int) {
        holder?.bind(data[position])
    }

    class ThemeViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        @Inject
        lateinit var ctx: Context

        init {
            App.appComponent.inject(this)
        }

        fun bind(elem: Pair<Category, Pair<Int, Int>>){
            itemView.tv_theme_formulas.text = elem.first.name
            itemView.tv_theme_tasks.text = ctx.getString(R.string.tv_tasks_template, elem.first.name)

            itemView.pb_theme_formulas.progress = elem.second.first
            itemView.pb_theme_tasks.progress = elem.second.second
        }
    }
}